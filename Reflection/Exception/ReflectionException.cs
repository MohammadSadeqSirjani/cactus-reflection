﻿using System.Runtime.Serialization;

namespace Reflection.Exception
{
    public class ReflectionException : System.Exception
    {
        public ReflectionException()
        {

        }

        public ReflectionException(string message) : base(message)
        {

        }

        public ReflectionException(string message, System.Exception innerException) : base(message, innerException)
        {

        }

        public ReflectionException(SerializationInfo info, StreamingContext context) : base(info, context)
        {

        }
    }
}
