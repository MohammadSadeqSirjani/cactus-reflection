﻿using System;
using System.Runtime.Serialization;

namespace Reflection.Exception
{
    public class WrongObjectReflectionException : ReflectionException
    {
        public Type Actual { get; }
        public Type Expected { get; }

        public WrongObjectReflectionException(Type actual, Type expected)
        {
            Actual = actual;
            Expected = expected;
        }

        public WrongObjectReflectionException(string message, Type actual, Type expected) : base(message)
        {
            Actual = actual;
            Expected = expected;
        }

        public WrongObjectReflectionException(string message, System.Exception innerException, Type actual,
            Type expected) : base(message, innerException)
        {
            Actual = actual;
            Expected = expected;
        }

        public WrongObjectReflectionException(SerializationInfo info, StreamingContext context, Type actual,
            Type expected) : base(info, context)
        {
            Actual = actual;
            Expected = expected;
        }
    }
}
